# TShopTax

## Usage instructions:

1. When you launch the Application you will see the screen with preloaded list of products.
2. You can select/deselect any product from this predefined list.
3. Selected products will be added to your basket automatically and marked with tick.
4. After click on button "Show details" you will see the receipt details.

### UnitTesting:

With unit tests I tried to cover almost all important parts of business logic:
I checked creation of objects, tax and total price calculation.
Also all cases described in the task were covered.
And one of the tests checks the rounded rule.


