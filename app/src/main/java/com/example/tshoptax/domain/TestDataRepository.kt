package com.example.tshoptax.domain

import com.example.tshoptax.entity.BaseTaxGood
import com.example.tshoptax.entity.Good
import com.example.tshoptax.entity.ImportedGood
import com.example.tshoptax.entity.TaxFreeGood

class TestDataRepository : TestDataSource {
    override fun getData(): List<Good> {
        var data = ArrayList<Good>()
        data.add(TaxFreeGood("6lb bag of Skittles", 16.0f))
        data.add(BaseTaxGood(TaxFreeGood("Walkman", 99.99f)))
        data.add(TaxFreeGood("bag of microwave Popcorn", 0.99f))
        data.add(ImportedGood(TaxFreeGood("bag of Vanilla-Hazelnut Coffee", 11.0f)))
        data.add(ImportedGood(BaseTaxGood(TaxFreeGood("Vespa", 15001.25f))))
        data.add(ImportedGood(TaxFreeGood("crate of Almond Snickers", 75.99f)))
        data.add(BaseTaxGood(TaxFreeGood("Discman", 55.0f)))
        data.add(ImportedGood(BaseTaxGood(TaxFreeGood("Bottle of Wine", 10.0f))))
        data.add(TaxFreeGood("300# bag of Fair-Trade Coffee", 997.99f))
        return data
    }
}