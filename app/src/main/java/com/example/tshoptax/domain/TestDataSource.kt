package com.example.tshoptax.domain

import com.example.tshoptax.entity.Good

interface TestDataSource {
    //This method will be used for getting data from back-end
    fun getData():List<Good>
}