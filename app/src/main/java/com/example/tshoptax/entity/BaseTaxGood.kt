package com.example.tshoptax.entity

class BaseTaxGood(good: Good) : GoodDecorator(good) {

    override fun getPriceWithTax(): Float {
        return getTax() + getCash()
    }

    override fun getTax(): Float {
        val value: Double = getCash() * 0.1
        return super.getTax() + Math.ceil((value / 0.05)).toFloat() * 0.05f
    }
}