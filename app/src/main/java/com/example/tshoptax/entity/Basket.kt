package com.example.tshoptax.entity

import java.util.ArrayList

class Basket {
    private var goods : MutableList<Good> = ArrayList()

    fun add(good: Good) {
        goods.add(good)
    }

    fun remove(good: Good) {
        goods.remove(good)
    }

    fun getGoods(): List<Good> {
        return goods
    }

    //Method for finding sum of taxes
    fun getTexesSum(): Float {
        var result = 0.0f
        goods.forEach { result += it.getTax() }
        return result
    }

    //Method for finding total sum
    fun getTotalSum(): Float {
        var result = 0.0f
        goods.forEach { result += it.getPriceWithTax() }
        return result
    }
}