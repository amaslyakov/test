package com.example.tshoptax.entity

interface Good {

    //Method for getting good name
    fun getName(): String

    //Method for getting good price
    fun getCash(): Float

    //Method for getting good tax
    fun getTax(): Float

    //Method for getting sum of price and tax
    fun getPriceWithTax(): Float
}