package com.example.tshoptax.entity

//This class is using to avoid inheritance
open class GoodDecorator(private val good: Good) : Good {

    override fun getName(): String {
        return good.getName()
    }

    override fun getCash(): Float {
        return good.getCash()
    }

    override fun getPriceWithTax(): Float {
        return good.getPriceWithTax()
    }

    override fun getTax(): Float {
        return good.getTax()
    }
}