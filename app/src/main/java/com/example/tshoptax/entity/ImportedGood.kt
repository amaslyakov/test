package com.example.tshoptax.entity

class ImportedGood(good: Good) : GoodDecorator(good) {

    override fun getName(): String {
        return "Imported " + super.getName()
    }

    override fun getTax(): Float {
        val value: Double = getCash() * 0.05
        return super.getTax() + Math.ceil((value / 0.05)).toFloat() * 0.05f
    }

    override fun getPriceWithTax(): Float {
        return getCash() + getTax()
    }

}