package com.example.tshoptax.entity

class TaxFreeGood(val title: String, val price: Float) : Good {

    override fun getName(): String {
        return title
    }

    override fun getCash(): Float {
        return price
    }

    override fun getTax(): Float {
        val value: Float = getCash() * 0
        return Math.round(value / 0.05) * 0.05f
    }

    override fun getPriceWithTax(): Float {
        return price + getTax()
    }

}