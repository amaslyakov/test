package com.example.tshoptax.extension

fun Float.toStringWith(digitsAfterComma: Int = 2): String {
    return String.format("%.${digitsAfterComma}f", this)
}