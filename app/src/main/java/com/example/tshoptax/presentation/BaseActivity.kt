package com.example.tshoptax.presentation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.example.tshoptax.R
import com.example.tshoptax.domain.TestDataRepository
import com.example.tshoptax.entity.Good
import com.example.tshoptax.extension.toStringWith
import com.example.tshoptax.presentation.adapter.GoodAdapter
import com.example.tshoptax.presentation.adapter.ResultAdapter
import kotlinx.android.synthetic.main.activity_main.*

class BaseActivity : AppCompatActivity(), View {

    val presenter = BasePresenter(TestDataRepository(), this)
    lateinit var adapter: GoodAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.getData()
        button.setOnClickListener { showResult() }
    }

    override fun showData(data: List<Good>) {
        list.layoutManager = LinearLayoutManager(this)
        adapter = GoodAdapter(data)
        list.adapter = adapter
    }

    //Method for showing the receipt details
    fun showResult() {
        val result = adapter.getCheckedItems()
        resultList.layoutManager = LinearLayoutManager(this)
        resultList.adapter = ResultAdapter(result.getGoods())
        taxes.text = String.format("Sales Taxes: ", result.getTexesSum().toStringWith())
        total.text = String.format("Total: ", result.getTotalSum().toStringWith())
    }


}