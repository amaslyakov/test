package com.example.tshoptax.presentation

import com.example.tshoptax.domain.TestDataRepository

class BasePresenter(val dataRepository: TestDataRepository, val view: View) {

    //Method for getting data from repository
    fun getData() {
        view.showData(dataRepository.getData())
    }
}