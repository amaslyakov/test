package com.example.tshoptax.presentation

import com.example.tshoptax.entity.Good

interface View {
    //This method is used for showing data on screen
    fun showData(data: List<Good>)
}