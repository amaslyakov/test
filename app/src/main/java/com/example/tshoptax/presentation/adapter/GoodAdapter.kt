package com.example.tshoptax.presentation.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import com.example.tshoptax.R
import com.example.tshoptax.entity.Basket
import com.example.tshoptax.entity.Good

class GoodAdapter(private val data: List<Good>) : RecyclerView.Adapter<GoodAdapter.ViewHolder>() {

    var checkedData = Basket()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val checkBox = LayoutInflater.from(p0.context).inflate(R.layout.list_item, p0, false) as CheckBox
        return ViewHolder(checkBox)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.checkBox.text = data[p1].getName()
        p0.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                checkedData.add(data[p1])
            } else {
                checkedData.remove(data[p1])
            }
        }
    }

    fun getCheckedItems(): Basket {
        return checkedData
    }


    class ViewHolder(val checkBox: CheckBox) : RecyclerView.ViewHolder(checkBox)
}