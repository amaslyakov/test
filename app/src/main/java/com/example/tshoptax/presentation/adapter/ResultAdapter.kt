package com.example.tshoptax.presentation.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.example.tshoptax.R
import com.example.tshoptax.entity.Good

class ResultAdapter(private val data: List<Good>) : RecyclerView.Adapter<ResultAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val checkBox = LayoutInflater.from(p0.context).inflate(R.layout.result_list_item, p0, false) as TextView
        return ViewHolder(checkBox)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.checkBox.text = data[p1].getName() + ": " + data[p1].getPriceWithTax()
    }

    class ViewHolder(val checkBox: TextView) : RecyclerView.ViewHolder(checkBox)
}