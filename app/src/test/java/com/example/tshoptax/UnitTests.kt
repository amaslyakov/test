package com.example.tshoptax

import com.example.tshoptax.entity.BaseTaxGood
import com.example.tshoptax.entity.Basket
import com.example.tshoptax.entity.ImportedGood
import com.example.tshoptax.entity.TaxFreeGood
import com.example.tshoptax.extension.toStringWith
import org.junit.Assert.assertEquals
import org.junit.Test

enum class TaxForTesting(val value: Float) {
    Free(0f), Base(10f), Imported(5f)
}

class UnitTests {

    @Test
    fun testTaxFreeGoodCreating() {
        val goodPrice = 20f
        val inputGoodName = "candy"
        val outputGoodName = inputGoodName
        val calculatedTaxInCash = goodPrice * (TaxForTesting.Free.value / 100)
        val priceWithTax = goodPrice + calculatedTaxInCash

        val good = TaxFreeGood(inputGoodName, goodPrice)

        assertEquals("Good getName() method return incorrect value", good.getName(), outputGoodName)
        assertEquals("Good getCash() method return incorrect value", good.getCash(), goodPrice)
        assertEquals("Good getTax() method return incorrect value", good.getTax(), calculatedTaxInCash)
        assertEquals("Good getPriceWithTax() method return incorrect value", good.getPriceWithTax(), priceWithTax)
    }

    @Test
    fun testBaseTaxGoodCreating() {
        val goodPrice = 20f
        val inputGoodName = "candy"
        val outputGoodName = inputGoodName
        val calculatedTaxInCash = goodPrice * ((TaxForTesting.Free.value + TaxForTesting.Base.value) / 100)
        val priceWithTax = goodPrice + calculatedTaxInCash

        val good = BaseTaxGood(TaxFreeGood(inputGoodName, goodPrice))

        assertEquals("Good getName() method return incorrect value", good.getName(), outputGoodName)
        assertEquals("Good getCash() method return incorrect value", good.getCash(), goodPrice)
        assertEquals("Good getTax() method return incorrect value", good.getTax(), calculatedTaxInCash)
        assertEquals("Good getPriceWithTax() method return incorrect value", good.getPriceWithTax(), priceWithTax)
    }

    @Test
    fun testImportedGoodCreating() {
        val goodPrice = 20f
        val inputGoodName = "candy"
        val outputGoodName = "Imported " + inputGoodName
        val calculatedTaxInCash = goodPrice * ((TaxForTesting.Free.value + TaxForTesting.Imported.value) / 100)
        val priceWithTax = goodPrice + calculatedTaxInCash

        val good = ImportedGood(TaxFreeGood(inputGoodName, goodPrice))

        assertEquals("Good getName() method return incorrect value", good.getName(), outputGoodName)
        assertEquals("Good getCash() method return incorrect value", good.getCash(), goodPrice)
        assertEquals("Good getTax() method return incorrect value", good.getTax(), calculatedTaxInCash)
        assertEquals("Good getPriceWithTax() method return incorrect value", good.getPriceWithTax(), priceWithTax)
    }

    @Test
    fun testImportedBaseTaxGoodCreating() {
        val goodPrice = 20f
        val inputGoodName = "candy"
        val outputGoodName = "Imported " + inputGoodName
        val calculatedTaxInCash = goodPrice * ((TaxForTesting.Free.value + TaxForTesting.Base.value + TaxForTesting.Imported.value) / 100)
        val priceWithTax = goodPrice + calculatedTaxInCash

        val good = ImportedGood(BaseTaxGood(TaxFreeGood(inputGoodName, goodPrice)))

        assertEquals("Good getName() method return incorrect value", good.getName(), outputGoodName)
        assertEquals("Good getCash() method return incorrect value", good.getCash(), goodPrice)
        assertEquals("Good getTax() method return incorrect value", good.getTax(), calculatedTaxInCash)
        assertEquals("Good getPriceWithTax() method return incorrect value", good.getPriceWithTax(), priceWithTax)
    }

    @Test
    fun testPriceWithTaxTaxDifferentPriceValueSizes() {
        val inputPrices: Array<Float> = arrayOf(0.99f, 2.0f, 90.0f, 103.87f, 1234.25f, 2456.76f, 12352.81f)
        val outputPrices: Array<Float> = arrayOf(1.14f, 2.3f, 103.5f, 119.47f, 1419.45f, 2825.31f, 14205.76f)
        val goodName = "Good name"

        for ((index, value) in inputPrices.withIndex()) {
            val good = ImportedGood(BaseTaxGood(TaxFreeGood("goodname", value)))
            assertEquals("Good getPriceWithTax() method return incorrect value", good.getPriceWithTax(), outputPrices[index])
        }
    }

    @Test
    fun testBasketOne() {
        // ------ Input ------
        // 1 16lb bag of Skittles at 16.00
        // 1 Walkman at 99.99
        // 1 bag of microwave Popcorn at 0.99

        // ------- Output -------
        // 1 16lb bag of Skittles: 16.00
        // 1 Walkman: 109.99
        // 1 bag of microwave Popcorn: 0.99

        // Sales Taxes: 10.00

        // Total: 126.98

        val salesTaxes = "10,00"
        val total = "126,98"

        val basket = Basket()
        basket.add(TaxFreeGood("6lb bag of Skittles", 16.0f))
        basket.add(BaseTaxGood(TaxFreeGood("Walkman", 99.99f)))
        basket.add(TaxFreeGood("bag of microwave Popcorn", 0.99f))

        assertEquals("Good getTexesSum() return incorrect value.", basket.getTexesSum().toStringWith(), salesTaxes)
        assertEquals("Good getTotalSum() return incorrect value.", basket.getTotalSum().toStringWith(), total)
    }

    @Test
    fun testBasketTwo() {
        // ----- Input -----
        // 1 imported bag of Vanilla-Hazelnut Coffee at 11.00
        // 1 Imported Vespa at 15,001.25

        // ------ Output ------
        // 1 imported bag of Vanilla-Hazelnut Coffee: 11.55
        // 1 Imported Vespa: 17,251.5

        // Sales Taxes: 2,250.8

        // Total: 17,263.05

        val salesTaxes = "2250,8"
        val total = "17263,05"

        val basket = Basket()
        basket.add(ImportedGood(TaxFreeGood("bag of Vanilla-Hazelnut Coffee", 11.0f)))
        basket.add(ImportedGood(BaseTaxGood(TaxFreeGood("Vespa", 15001.25f))))

        assertEquals("Good getTexesSum() return incorrect value.", basket.getTexesSum().toStringWith(1), salesTaxes)
        assertEquals("Good getTotalSum() return incorrect value.", basket.getTotalSum().toStringWith(), total)
    }

    @Test
    fun testBasketThree() {
        // ----- Input -----
        // 1 imported crate of Almond Snickers at 75.99
        // 1 Discman at 55.00
        // 1 Imported Bottle of Wine at 10.00
        // 1 300# bag of Fair-Trade Coffee at 997.99


        // ------ Output ------
        // 1 imported crate of Almond Snickers: 79.79
        // 1 Discman: 60.5
        // 1 imported bottle of Wine: 11.5
        // 1 300# Bag of Fair-Trade Coffee: 997.99

        // Sales Taxes: 10.8

        // Total: 1,149.78

        val salesTaxes = "10,80"
        val total = "1149,78"

        val basket = Basket()
        basket.add(ImportedGood(TaxFreeGood("crate of Almond Snickers", 75.99f)))
        basket.add(BaseTaxGood(TaxFreeGood("Discman", 55.0f)))
        basket.add(ImportedGood(BaseTaxGood(TaxFreeGood("Bottle of Wine", 10.0f))))
        basket.add(TaxFreeGood("300# bag of Fair-Trade Coffee", 997.99f))


        assertEquals("Good getTexesSum() return incorrect value.", basket.getTexesSum().toStringWith(), salesTaxes)
        assertEquals("Good getTotalSum() return incorrect value.", basket.getTotalSum().toStringWith(), total)
    }
}
